var gl;
var canvas;
var shaders = {};
var shaderNames = [];
var shaderId;
var textures = {};
var textureNames = [];
var textureId = -1;
var model;
var camera;
var textRenderer, modelRenderer, bbRenderer;
var bgColor, textColor;
var levelOfDetail;
var stacks, slices;
var showOverlay = true;
var isIE = false;

var Panel = {
    NONE: -1, SURFACES: 0, HELP: 1
};

var panelId = Panel.NONE;
var lastPanelId = panelId;

var requestId;
var lastTime, lastDebugTime;

var keysDown = [];
var keysPressed = [];
var ticks, frames;
var debugTicks, debugFrames;
var unprocessedTicks, tickCount;

function getBooleanArray(size, value)
{
    return Array.apply(null, new Array(size)).map(Boolean.prototype.valueOf, value);
}

function onContextLost(event)
{
    event.preventDefault();
    cancelRequestAnimationFrame(requestId);
}

function onContextRestored(event)
{
    event.preventDefault();
    init();
}

function onKeyDown(event)
{
    event = event || window.event;
    event.preventDefault();

    if (event.keyCode < keysDown.length)
    {
        if (!keysDown[event.keyCode])
        {
            keysDown[event.keyCode] = true;
            keysPressed[event.keyCode] = true;
        }
    }
}

function onKeyUp(event)
{
    event = event || window.event;
    event.preventDefault();

    if (event.keyCode < keysDown.length) {
        keysDown[event.keyCode] = false;
    }
}

function onMouseDown(event)
{
    event = event || window.event;
    event.preventDefault();

    if (document.pointerLockElement !== canvas ||
        document.mozPointerLockElement !== canvas ||
        document.webkitPointerLockElement !== canvas) {
        canvas.requestPointerLock();
    }
}

function onMouseMove(event)
{
    event = event || window.event;
    event.preventDefault();

    if (document.pointerLockElement === canvas ||
        document.mozPointerLockElement === canvas ||
        document.webkitPointerLockElement === canvas)
    {
        var movementX = event.movementX ||
                        event.mozMovementX ||
                        event.webkitMovementX || 0.0;

        var movementY = event.movementY ||
                        event.mozMovementY ||
                        event.webkitMovementY || 0.0;

        var deltaX = movementX * MOUSE_SCALE;
        var deltaY = movementY * MOUSE_SCALE;
		
        camera.rotate(-deltaX, -deltaY);
    }
}

function onSliderClick()
{
    var activeSurfaces = [];

    $("input[type=checkbox]:checked").each(function()
    {
        var value = parseInt($(this).attr("value"));
        activeSurfaces.push(value);
    });

    model.activeSurfaces = activeSurfaces;
    model.updateBoundingBox();
}

function onSelectAllClick()
{
    $("input[type=checkbox]").each(function() {
        $(this).prop("checked", true);
    });

    onSliderClick();
}

function onDeselectAllClick()
{
    $("input[type=checkbox]").each(function() {
        $(this).prop("checked", false);
    });

    onSliderClick();
}

function onBrowseClick(mode) {
    $("#browse" + mode + "Input").trigger("click");
}

function onBrowseIgesInput(file)
{
    var extension;

    if (file.name.endsWith(Iges.EXTENSION)) {
        extension = Iges.EXTENSION;
    }

    var name = file.name.slice(0, -(extension.length + 1));

    $("#welcomePanel").css("display", "none");
    $("#loadingPanel").css("display", "inline-block");

    var fileReader = new FileReader();

    fileReader.onload = function()
    {
        model = new Model(name);
        model.parse(fileReader.result);

        init();
    };

    fileReader.readAsText(file, "ASCII");
}

var xcLength, xcCount;

function onBrowseXCInput(files)
{
    var i, file;
    var name = "XCModel";
    var xcFiles = [];

    for (i = 0; i < files.length; i++)
    {
        file = files[i];
        var fileName = file.name;

        if (fileName.endsWith(XCModel.DB_EXTENSION) ||
            fileName.endsWith(XCModel.DBE_EXTENSION)) {
            xcFiles.push(file);
        }

        if (fileName.endsWith(XCModel.OBJ_EXTENSION)) {
            name = fileName.slice(0, -(XCModel.OBJ_EXTENSION.length + 1));
        }
    }

    if (xcFiles.length == 0) {
        return;
    }

    $("#welcomePanel").css("display", "none");
    $("#loadingPanel").css("display", "inline-block");

    xcLength = xcFiles.length;
    xcCount = 0;
    model = new Model(name);

    for (i = 0; i < xcFiles.length; i++)
    {
        (function(index)
        {
            file = xcFiles[index];
            var extension;
            var fileReader = new FileReader();

            if (file.name.endsWith(XCModel.DB_EXTENSION)) {
                extension = XCModel.DB_EXTENSION;
            }

            if (file.name.endsWith(XCModel.DBE_EXTENSION)) {
                extension = XCModel.DBE_EXTENSION;
            }

            var name = file.name.slice(0, -(extension.length + 1));

            fileReader.onload = function ()
            {
                var surface = XCModel.parse(fileReader.result, name, extension);

                if (surface instanceof Surface)
                {
                    model.surfaces[xcCount] = surface;
                    model.activeSurfaces.push(xcCount);
                }

                xcCount++;

                if (xcCount == xcLength) {
                   init();
                }
            };

            fileReader.readAsText(file, "ASCII");
        })(i);
    }
}

function isInternetExplorer() {

    var userAgent = window.navigator.userAgent;
    var msie = userAgent.indexOf("MSIE ");

    return (msie > 0 || !!navigator.userAgent.match(/Trident.*rv:11\./));
}

$(document).ready(function()
{
    if (!webGLBootstrap())
    {
        $("#loadingPanel").css("display", "none");
        $("#noWebGLPanel").css("display", "inline-block");

        return;
    }

    isIE = isInternetExplorer();

    if (!isIE)
    {
        canvas.requestPointerLock = canvas.requestPointerLock ||
                                    canvas.mozRequestPointerLock ||
                                    canvas.webkitRequestPointerLock;

        document.exitPointerLock =  document.exitPointerLock ||
                                    document.mozExitPointerLock ||
                                    document.webkitExitPointerLock;
    }

    /* Keyboard management */
    
    keysDown = getBooleanArray(256, false);
    keysPressed = getBooleanArray(256, false);

    $.when
    (
        $.getScript("lib/colors.js"),
        $.getScript("lib/gl-matrix.min.js"),
        $.getScript("lib/nurbs.js"),
        $.getScript("lib/iges.js"),
        $.getScript("lib/xcmodel.js"),
        $.getScript("lib/sprintf.min.js"),
        $.getScript("js/shader.js"),
        $.getScript("js/texture.js"),
        $.getScript("js/renderer.js"),
        $.getScript("js/model.js"),
        $.getScript("js/camera.js")
    ).then(function()
    {
        var $deferreds = [];
        var name;

        /* Shader */

        shaders.diffuse = new Shader("diffuse");
        shaders.phong_per_vertex = new Shader("phongv");
        shaders.phong_per_fragment = new Shader("phongf");
        shaders.blinn_phong = new Shader("blinnphong");
        shaders.toon = new Shader("toon");
        shaders.texture = new Shader("texture");
        shaders.text = new Shader("text");
        shaders.plain = new Shader("plain");

        for (name in shaders)
        {
            if (shaders.hasOwnProperty(name))
            {
                shaderNames.push(name);
                $deferreds.push(shaders[name].loadAndCompile());
            }
        }

        shaderId = 2;

        /* Texture */

        textures.bricks = new Texture("bricks");
        textures.metal = new Texture("metal");
        textures.uv = new Texture("uv");
        textures.wood = new Texture("wood");
        textures.ascii = new Texture("ascii_mojang", "fonts");

        for (name in textures)
        {
            if (textures.hasOwnProperty(name))
            {
                var minFilter, maxFilter;

                if (name !== "ascii")
                {
                    minFilter = gl.LINEAR_MIPMAP_NEAREST;
                    maxFilter = gl.LINEAR;

                    textureNames.push(name);
                } else {
                    minFilter = maxFilter = gl.NEAREST;
                }

                $deferreds.push(textures[name].load(minFilter, maxFilter));
            }
        }

        textureId = 3;
        levelOfDetail = 5;
        stacks = slices = levelOfDetail * LOD_MULTIPLIER;

        /* Camera */
        
        camera = new Camera(CAMERA_SCALE);
        camera.setFOV(FOV);

        $.when.apply($, $deferreds).then(function()
        {
            /* Renderer */
            textRenderer = new TextRenderer(shaders.text, textures.ascii);
            modelRenderer = new ModelRenderer(shaders.plain);
            bbRenderer = new BBRenderer(shaders.plain);

            textRenderer.loadGlyphSizes().then(function()
            {
                $("#loadingPanel").css("display", "none");
                $("#welcomePanel").css("display", "inline-block");
            });
        });
    });
});

function webGLBootstrap()
{
    console.log(USER_AGENT + navigator.userAgent);

    var isSupported = (window.WebGLRenderingContext != null);
    console.log(WEBGL_SUPPORT + ((isSupported) ? "PRESENTE" : "ASSENTE"));

    if (!isSupported) {
        return false;
    }

    canvas = document.getElementById("glCanvas");

    var isEnabled = ((gl = getWebGLContext(canvas)) != null);
    console.log(WEBGL_STATUS + ((isEnabled) ? "ABILITATA" : "DISABILITATA"));

    if (!isEnabled) {
        return false;
    }

    console.log(WEBGL_VERSION + gl.getParameter(gl.VERSION));

    var angleStatus = getANGLEStatus();
    var angleText;

    switch (angleStatus)
    {
        case ANGLE.NOT_IMPLEMENTED:
            angleText = "NON IMPLEMENTATO";
        break;

        case ANGLE.DISABLED:
            angleText = "DISABILITATO";
        break;

        case ANGLE.ENABLED:
            angleText = "ABILITATO";
        break;
    }

    console.log(ANGLE_STATUS + angleText);

    var extensions = gl.getSupportedExtensions();
    console.log(ENABLED_EXTS + extensions.length);

    var hasUint32Support = (extensions.indexOf(UINT32_EXT) != -1) ? "PRESENTE" : "ASSENTE";
    console.log(UINT32_SUPPORT + hasUint32Support);

    return true;
}

function getANGLEStatus()
{
    if (navigator.platform !== WIN32) {
        return ANGLE.NOT_IMPLEMENTED;
    }

    var renderer = gl.getParameter(gl.RENDERER);

    if (renderer === IE_RENDERER) {
        return ANGLE.NOT_IMPLEMENTED;
    }

    var aliasedLineWidthRange = gl.getParameter(gl.ALIASED_LINE_WIDTH_RANGE);

    if (aliasedLineWidthRange[0] !== 1  ||
        aliasedLineWidthRange[1] !== 1) {
        return ANGLE.DISABLED;
    }

    return ANGLE.ENABLED;
}

function getWebGLContext(canvas)
{
    var context = null;

    for (var i = 0; i < WEBGL_NAMES.length; i++)
    {
        try {
            if (context = canvas.getContext(WEBGL_NAMES[i])) {
                return context;
            }
        } catch (e) {}
    }

    return context;
}

function onModelClick(name)
{
    $("#welcomePanel").css("display", "none");
    $("#loadingPanel").css("display", "inline-block");

    model = new Model(name);
    model.loadAndParse().then(function(){
        init();
    });
}

function parseCSSColor(color)
{
    var parts = color.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
    return vec3.fromValues(parts[1] / 255, parts[2] / 255, parts[3] / 255);
}

function init()
{
    var $surfaceTable = $("#surfaceTable");
    $surfaceTable.empty();

    for (var i = 0; i < model.surfaces.length; i++)
    {
        var name = model.surfaces[i].name;
        var row = sprintf(SURFACE_ROW, name, i, i, i);

        $surfaceTable.append(row);
    }

    if (!isIE)
    {
        canvas.onmousedown = onMouseDown;
        canvas.onmousemove = onMouseMove;
    }

    canvas.addEventListener("webglcontextlost", onContextLost, false);
    canvas.addEventListener("webglcontextrestored", onContextRestored, false);
    document.onkeydown = onKeyDown;
    document.onkeyup = onKeyUp;
    
    $("#helpMsg").css("display", "block");

    /* Colors */
    var $body = $("body");

    bgColor = parseCSSColor($body.css("background-color"));
    textColor = parseCSSColor($body.css("color"));

    gl.clearColor(bgColor[0], bgColor[1], bgColor[2], 1.0);
    gl.clearDepth(1.0);

    gl.enable(gl.DEPTH_TEST);

    var position = vec3.fromValues(0.0, 0.0, 50.0);
    camera.setPosition(position);

    ticks = frames = 0;
    debugTicks = debugFrames = 0;
    tickCount = -1;
    unprocessedTicks = 0;

    $("#loadingPanel").css("display", "none");

    lastTime = lastDebugTime = new Date().getTime();
    requestId = requestAnimationFrame(loop);
}

function loop()
{
    var now = new Date().getTime();
    unprocessedTicks += ((now - lastTime) / 1000.0) * TICKS_PER_SECOND;
    lastTime = now;

    if (unprocessedTicks > MAX_UNPROCESSED_TICKS) {
        unprocessedTicks = MAX_UNPROCESSED_TICKS;
    }

    while (unprocessedTicks >= 1.0)
    {
        tick();
        ticks++;

        unprocessedTicks -= 1.0;
    }

    render();
    frames++;

    if ((now - lastDebugTime) > 1000)
    {
        lastDebugTime = now;

        debugTicks = ticks;
        debugFrames = frames;

        ticks = frames = 0;
    }

    requestId = requestAnimationFrame(loop);
}

function tickKeyboard()
{
	var fov;

    if (keysPressed[116]) {                         // F5
        document.location.reload(true);
    }

    if (keysDown[87] && panelId == Panel.NONE) {    // W
        camera.move(CameraMovement.FORWARD);
    }

    if (keysDown[83] && panelId == Panel.NONE) {    // S
        camera.move(CameraMovement.BACKWARD);
    }

    if (keysDown[65] && panelId == Panel.NONE) {    // A
        camera.move(CameraMovement.LEFT);
    }

    if (keysDown[68] && panelId == Panel.NONE) {    // D
        camera.move(CameraMovement.RIGHT);
    }

    if (keysDown[16] && panelId == Panel.NONE) {    // SHIFT
        camera.move(CameraMovement.DOWN);
    }

    if (keysDown[32] && panelId == Panel.NONE) {    // SPACE
        camera.move(CameraMovement.UP);
    }

    if (keysDown[38] && panelId == Panel.NONE) {    // Up arrow
        camera.rotate(0.0, DEFAULT_ROTATION);
    }

    if (keysDown[40] && panelId == Panel.NONE) {    // Down arrow
        camera.rotate(0.0, -DEFAULT_ROTATION);
    }

    if (keysDown[37] && panelId == Panel.NONE) {    // Left arrow
        camera.rotate(DEFAULT_ROTATION, 0.0);
    }

    if (keysDown[39] && panelId == Panel.NONE) {    // Right arrow
        camera.rotate(-DEFAULT_ROTATION, 0.0);
    }

    if (keysPressed[49] && panelId == Panel.NONE)   // 1
    {
        if (modelRenderer.renderMode == RenderMode.SOLID) {
            shaderId = 0;
        }
    }

    if (keysPressed[50] && panelId == Panel.NONE)   // 2
    {
        if (modelRenderer.renderMode == RenderMode.SOLID) {
            shaderId = 1;
        }
    }

    if (keysPressed[51] && panelId == Panel.NONE)   // 3
    {
        if (modelRenderer.renderMode == RenderMode.SOLID) {
            shaderId = 2;
        }
    }

    if (keysPressed[52] && panelId == Panel.NONE)   // 4
    {
        if (modelRenderer.renderMode == RenderMode.SOLID) {
            shaderId = 3;
        }
    }

    if (keysPressed[53] && panelId == Panel.NONE)   // 5
    {
        if (modelRenderer.renderMode == RenderMode.SOLID) {
            shaderId = 4;
        }
    }

    if (keysPressed[54] && panelId == Panel.NONE)   // 6
    {
        if (modelRenderer.renderMode == RenderMode.SOLID) {
            shaderId = 5;
        }
    }

    if (keysPressed[82] && panelId == Panel.NONE)   // R
    {
        if (modelRenderer.renderMode == RenderMode.SOLID && shaderId == 5) {
            textureId = (textureId + 1) % textureNames.length;
        }
    }

    if (keysPressed[70] && panelId == Panel.NONE)   // F
    {
        camera.lightLocked = !camera.lightLocked;
        camera.update(true);
    }

    if (keysPressed[13] && panelId == Panel.NONE)   // ENTER
    {
        if (modelRenderer.renderMode == RenderMode.SOLID) {
            modelRenderer.renderMode = RenderMode.WIREFRAME;
        } else {
            modelRenderer.renderMode = RenderMode.SOLID;
        }
    }

    if (keysPressed[67] && panelId == Panel.NONE) { // C
        model.canColorTick = !model.canColorTick;
    }

    if (keysPressed[33] && panelId == Panel.NONE)   // PgUP
    {
        levelOfDetail++;

        if (levelOfDetail > MAX_LOD) {
            levelOfDetail = MAX_LOD;
        }

        stacks = slices = (levelOfDetail * LOD_MULTIPLIER);
        model.updated = false;
    }

    if (keysPressed[34] && panelId == Panel.NONE)   // PgDOWN
    {
        levelOfDetail--;

        if (levelOfDetail < MIN_LOD) {
            levelOfDetail = MIN_LOD;
        }

        stacks = slices = (levelOfDetail * LOD_MULTIPLIER);
        model.updated = false;
    }

    if (keysPressed[88] && panelId == Panel.NONE) { // X
        showOverlay = !showOverlay;
    }

    if (keysPressed[81] && panelId == Panel.NONE) { // Q
        modelRenderer.colorMode = Math.abs(1 - modelRenderer.colorMode);
    }

    if (keysPressed[90] && panelId == Panel.NONE)   // Z
    {
        camera.type = Math.abs(1 - camera.type);
        camera.update(true);
    }

    if (keysPressed[69])                            // E
    {
        if (panelId != Panel.NONE)
        {
            if (panelId == Panel.SURFACES) {
                panelId = Panel.NONE;
            }
        } else {
            panelId = Panel.SURFACES;
        }
    }

    if (keysPressed[72])                            // H
    {
        if (panelId != Panel.NONE)
        {
            if (panelId == Panel.HELP) {
                panelId = Panel.NONE;
            }
        } else {
            panelId = Panel.HELP;
        }
    }
    
    if ((keysPressed[107] || keysPressed[187]) &&   // Plus
        panelId == Panel.NONE)
    {
    	fov = camera.fov + CameraUtils.FOV_DELTA;
    	camera.setFOV(fov);
    }
    
    if ((keysPressed[109] || keysPressed[189]) &&   // Minus
    	panelId == Panel.NONE)
    {
    	fov = camera.fov - CameraUtils.FOV_DELTA;
    	camera.setFOV(fov);
    }

    keysPressed = getBooleanArray(256, false);
}

function tickPanel()
{
    if (lastPanelId != panelId)
    {
        switch (panelId)
        {
            case Panel.NONE:
            {
                if (!isIE) {
                    canvas.requestPointerLock();
                }

                $("#surfacePanel").css("display", "none");
                $("#helpPanel").css("display", "none");
            } break;

            case Panel.SURFACES:
            {
                if (!isIE) {
                    document.exitPointerLock();
                }

                $("#surfacePanel").css("display", "inline-block");
                $("#helpPanel").css("display", "none");
            } break;

            case Panel.HELP:
            {
                if (!isIE) {
                    document.exitPointerLock();
                }

                $("#surfacePanel").css("display", "none");
                $("#helpPanel").css("display", "inline-block");
            } break;
        }

        lastPanelId = panelId;
    }
}

function tick()
{
    tickCount++;

    tickKeyboard();
    tickPanel();

    model.tick();
}

function renderOverlay()
{
    var i, x;
    var xMargin, yMargin;
    var xyzDigits = 3;
    var angleDigits = 1;

    var size = 2;
    textRenderer.prepare(size);

    var spacing = (textRenderer.defaultHeight / 2 | 0) * 3;
    xMargin = yMargin = ((spacing / 8) | 0) * 4;
    var px = camera.position[0].toFixed(xyzDigits);
    var py = camera.position[1].toFixed(xyzDigits);
    var pz = camera.position[2].toFixed(xyzDigits);
    var lx = camera.lightPosition[0].toFixed(xyzDigits);
    var ly = camera.lightPosition[1].toFixed(xyzDigits);
    var lz = camera.lightPosition[2].toFixed(xyzDigits);
    var lax = camera.lookingAt[0].toFixed(xyzDigits);
    var lay = camera.lookingAt[1].toFixed(xyzDigits);
    var laz = camera.lookingAt[2].toFixed(xyzDigits);
    var pitch = CameraUtils.degrees(camera.pitch).toFixed(angleDigits);
    var yaw = CameraUtils.degrees(camera.yaw).toFixed(angleDigits);
    var direction = CameraUtils.getYawDirection(camera.yaw);
    var light = (camera.lightLocked && (tickCount % 60 > 30)) ? "" : (lx + " / " + ly + " / " + lz);
    var mode = (modelRenderer.renderMode == RenderMode.SOLID) ? "solid" : "wireframe";
    var cameraType = (camera.type == CameraType.FREE) ? "free" : "bound";
    var fov = camera.fov.toFixed(angleDigits);

    var topLeft =
    [
        "Camera: " + px + " / " + py + " / " + pz,
        "Light source: " + light,
        "Looking at: " + lax + " / " + lay + " / " + laz,
        "Direction: " + direction + " (" + yaw + " / " + pitch + ")",
        "Field of View: " + fov,
       	"Camera: " + cameraType
    ];

    for (i = 0; i < topLeft.length; i++) {
        textRenderer.render(topLeft[i], xMargin, yMargin + (i * spacing), textColor, true);
    }

    var topRight =
    [
        debugFrames + " fps (" + debugTicks + " ticks)",
        "Model: " + model.name + " (" + stacks + "x" + slices + ")",
        "Surfaces: " + model.activeSurfaces.length + "/" + model.surfaces.length,
        "Render mode: " + mode
    ];

    if (modelRenderer.renderMode == RenderMode.SOLID)
    {
        topRight.push("Shader: " + shaderNames[shaderId]);

        if (shaderId == 5) {
            topRight.push("Texture: " + textureNames[textureId]);
        }
    }

    var scaledWidth = (canvas.width / size) | 0;

    for (i = 0; i < topRight.length; i++)
    {
        x = (scaledWidth - textRenderer.getTextWidth(topRight[i])) - xMargin;
        textRenderer.render(topRight[i], x, yMargin + (i * spacing), textColor, true);
    }
}

function renderModel()
{
    if (!model.updated)
    {
        model.update(stacks, slices);
        return;
    }

    var shader = shaders[shaderNames[shaderId]];

    if (shaderId == 5)
    {
        var texture = textures[textureNames[textureId]];
        modelRenderer.prepare(camera, shader, texture);
    } else {
        modelRenderer.prepare(camera, shader);
    }

    modelRenderer.render(model);

    if (modelRenderer.renderMode == RenderMode.WIREFRAME)
    {
        bbRenderer.prepare(camera);
        bbRenderer.render(model);
    }
}

function render()
{
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    camera.update();

    renderModel();

    if (showOverlay) {
        renderOverlay();
    }
}
