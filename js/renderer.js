/**
 * ModelRenderer
 */

var ColorMode = {
    MODEL: 0, SURFACES: 1
};

var RenderMode = {
    WIREFRAME: gl.LINES, SOLID: gl.TRIANGLES
};

var ModelRenderer = function(shader)
{
    this.vertexBuffer = gl.createBuffer();
    this.indexBuffer = gl.createBuffer();
    this.shader = null;
    this.altShader = shader;
    this.viewMatrix = mat4.create();
    this.colorMode = ColorMode.MODEL;
    this.renderMode = RenderMode.SOLID;
};

ModelRenderer.prototype.prepare = function(camera, shader, texture)
{
    if (texture instanceof Texture) {
        gl.bindTexture(gl.TEXTURE_2D, texture.texture);
    }

    if (this.renderMode == RenderMode.SOLID) {
        this.shader = shader;
    } else {
        this.shader = this.altShader;
    }

    this.shader.use();

    this.viewMatrix = camera.viewMatrix;
    this.shader.uniformMatrix4fv("uMatrix.p", false, camera.projectionMatrix);

    var lightPosition = vec3.create();
    vec3.transformMat4(lightPosition, camera.lightPosition, this.viewMatrix);

    this.shader.uniform3fv("uLightPosition", lightPosition);
};

ModelRenderer.prototype.renderSurface = function(surface)
{
    if (this.colorMode == ColorMode.SURFACES) {
        this.shader.uniform3fv("uColor", surface.rgbColor);
    }

    gl.bindBuffer(gl.ARRAY_BUFFER, this.vertexBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, surface.glVertexData, gl.STATIC_DRAW);

    this.shader.vertexAttribPointer("aPosition", VEC3_LENGTH, VERTEX_DATA_LENGTH, 0);
    this.shader.vertexAttribPointer("aNormal", VEC3_LENGTH, VERTEX_DATA_LENGTH, VEC3_LENGTH);
    this.shader.vertexAttribPointer("aTexCoord", VEC2_LENGTH, VERTEX_DATA_LENGTH, 2 * VEC3_LENGTH);

    var indices;
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.indexBuffer);

    switch (this.renderMode)
    {
        case RenderMode.WIREFRAME: {
            indices = surface.glLineIndices;
        } break;

        case RenderMode.SOLID: {
            indices = surface.glTriangleIndices;
        } break;
    }

    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, indices, gl.STATIC_DRAW);
    gl.drawElements(this.renderMode, indices.length, gl.UNSIGNED_SHORT, 0);
};

ModelRenderer.prototype.render = function(model)
{
    var renderer = this;

    var mvMatrix = mat4.create();
    mat4.multiply(mvMatrix, this.viewMatrix, model.modelMatrix);

    var normalMatrix = mat3.create();

    mat3.fromMat4(normalMatrix, mvMatrix);
    mat3.invert(normalMatrix, normalMatrix);
    mat3.transpose(normalMatrix, normalMatrix);

    this.shader.uniformMatrix4fv("uMatrix.mv", false, mvMatrix);
    this.shader.uniformMatrix3fv("uMatrix.n", false, normalMatrix);

    if (this.colorMode == ColorMode.MODEL) {
        this.shader.uniform3fv("uColor", model.rgbColor);
    }

    model.activeSurfaces.forEach(function(index)
    {
        var surface = model.surfaces[index];
        renderer.renderSurface(surface);
    });
};

/**
 * BBRenderer
 */

var BBRenderer = function(shader)
{
    this.vertexBuffer = gl.createBuffer();
    this.indexBuffer = gl.createBuffer();
    this.shader = shader;
    this.viewMatrix = mat4.create();
    this.color = vec3.fromValues(0.5, 0.5, 0.5);
    this.centerColor = vec3.fromValues(0.25, 0.25, 0.25);
};

BBRenderer.prototype.prepare = function(camera)
{
    this.shader.use();

    this.viewMatrix = camera.viewMatrix;
    this.shader.uniformMatrix4fv("uMatrix.p", false, camera.projectionMatrix);
};

BBRenderer.prototype.render = function(model)
{
    var boundingBox = model.boundingBox;

    var mvMatrix = mat4.create();
    mat4.multiply(mvMatrix, this.viewMatrix, model.modelMatrix);

    this.shader.uniformMatrix4fv("uMatrix.mv", false, mvMatrix);
    this.shader.uniform3fv("uColor", this.color);

    gl.bindBuffer(gl.ARRAY_BUFFER, this.vertexBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, boundingBox.glPoints, gl.STATIC_DRAW);
    this.shader.vertexAttribPointer("aPosition", VEC3_LENGTH, VEC3_LENGTH, 0);

    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.indexBuffer);
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, boundingBox.glLineIndices, gl.STATIC_DRAW);

    gl.drawElements(gl.LINES, boundingBox.glLineIndices.length, gl.UNSIGNED_BYTE, 0);

    this.shader.uniform3fv("uColor", this.centerColor);
    gl.drawArrays(gl.POINTS, 8, 1);
};

/**
 *  TextRenderer
 */

var TEXT_VERTICES =
[
    0.0,  0.0,  0.0,
    0.0,  1.0,  0.0,
    1.0,  1.0,  0.0,
    1.0,  0.0,  0.0
];

var TEXT_INDICES = [
    0,  1,  2,      0,  2,  3
];

var TextRenderer = function(shader, texture)
{
    this.shader = shader;
    this.vertexBuffer = gl.createBuffer();
    this.indexBuffer = gl.createBuffer();
    this.texture = texture;

    this.vertexArray = new Float32Array(TEXT_VERTICES);
    this.indexArray = new Uint8Array(TEXT_INDICES);

    this.orthoMatrix = mat4.create();
    mat4.ortho(this.orthoMatrix, 0, gl.canvas.width, gl.canvas.height, 0, -0.1, 0.1);

    this.glyphSizes = {};
    this.glyphOffset = 0;
    this.defaultHeight = 0;
};

TextRenderer.prototype.loadGlyphSizes = function()
{
    var url = FONT_PATH + "/glyph_sizes.json";
    var renderer = this;

    return $.getJSON(url, function(data)
    {
        renderer.glyphSizes = data[renderer.texture.name];
        renderer.glyphOffset = renderer.glyphSizes["glyph_offset"];
        renderer.defaultHeight = renderer.glyphSizes["default_height"];
    });
};

TextRenderer.prototype.getGlyphWidth = function(codeUnit)
{
    if (codeUnit >= 0 && codeUnit < 256)
    {
        var width = this.glyphSizes["width"][codeUnit.toString()];

        if (width == null) {
            width = this.glyphSizes["default_width"];
        }

        return width;
    }

    return 0;
};

TextRenderer.prototype.getTextWidth = function(text)
{
    var textWidth = 0;

    for (var i = 0; i < text.length; i++)
    {
        var codeUnit = text.charCodeAt(i);
        var width = this.getGlyphWidth(codeUnit);

        if (i != (text.length - 1)) {
            width += this.glyphOffset;
        }

        textWidth += width;
    }

    return textWidth;
};

TextRenderer.prototype.prepare = function(size)
{
    this.shader.use();
    gl.bindTexture(gl.TEXTURE_2D, this.texture.texture);

    var viewMatrix = mat4.create();
    mat4.scale(viewMatrix, viewMatrix, vec3.fromValues(size, size, 1.0));

    gl.bindBuffer(gl.ARRAY_BUFFER, this.vertexBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, this.vertexArray, gl.STATIC_DRAW);
    this.shader.vertexAttribPointer("aPosition", VEC3_LENGTH, VEC3_LENGTH, 0);

    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.indexBuffer);
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, this.indexArray, gl.STATIC_DRAW);

    this.shader.uniformMatrix4fv("uOrthoMatrix", false, this.orthoMatrix);
    this.shader.uniformMatrix4fv("uViewMatrix", false, viewMatrix);
};

TextRenderer.prototype.renderChar = function(
    codeUnit, position, color, dropShadow)
{
    dropShadow = (typeof(dropShadow) === "undefined") ? true : dropShadow;
    var width = this.getGlyphWidth(codeUnit);

    if (codeUnit >= 0 && codeUnit < 256)
    {
        var uOffset = (codeUnit % 16) * 8;
        var vOffset = (codeUnit / 16 | 0) * 8;

        var textureMatrix = mat4.create();

        mat4.scale(textureMatrix, textureMatrix, vec3.fromValues(1.0 / this.texture.width, 1.0 / this.texture.height, 0.0));
        mat4.translate(textureMatrix, textureMatrix, vec3.fromValues(uOffset, vOffset, 0.0));
        mat4.scale(textureMatrix, textureMatrix, vec3.fromValues(width, this.defaultHeight, 0.0));

        var modelMatrix = mat4.create();

        mat4.translate(modelMatrix, modelMatrix, vec3.fromValues(position[0], position[1], position[2]));
        mat4.scale(modelMatrix, modelMatrix, vec3.fromValues(width, this.defaultHeight, 0.0));

        this.shader.uniformMatrix4fv("uModelMatrix", false, modelMatrix);
        this.shader.uniformMatrix4fv("uTextureMatrix", false, textureMatrix);
        this.shader.uniform4fv("uColor", vec4.fromValues(color[0], color[1], color[2], 1.0));

        gl.drawElements(gl.TRIANGLES, this.indexArray.length, gl.UNSIGNED_BYTE, 0);

        if (dropShadow)
        {
            mat4.identity(modelMatrix);

            mat4.translate(modelMatrix, modelMatrix, vec3.fromValues(position[0] + 1, position[1] + 1, position[2]));
            mat4.scale(modelMatrix, modelMatrix, vec3.fromValues(width, this.defaultHeight, 0.0));

            this.shader.uniformMatrix4fv("uModelMatrix", false, modelMatrix);
            this.shader.uniform4fv("uColor", vec4.fromValues(color[0], color[1], color[2], 0.4));

            gl.drawElements(gl.TRIANGLES, this.indexArray.length, gl.UNSIGNED_BYTE, 0);
        }
    }

    return (width + this.glyphOffset);
};

TextRenderer.prototype.render = function(
    text, x, y, color, dropShadow)
{
    dropShadow = (typeof(dropShadow) === "undefined") ? true : dropShadow;
    var position = vec3.fromValues(x, y, 0.0);

    for (var i = 0; i < text.length; i++)
    {
        var codeUnit = text.charCodeAt(i);
        position[0] += this.renderChar(codeUnit, position, color, dropShadow);
    }
};