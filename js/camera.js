var CameraMovement = {
    UP: 0, DOWN: 1, LEFT: 2, RIGHT: 3, FORWARD: 4, BACKWARD: 5
};

var CameraDirection = {
    NORTH: 'N', EAST: 'E', SOUTH: 'S', WEST: 'W'
};

var CameraType = {
    FREE: 0, BOUND: 1
};

var CameraUtils = {};

CameraUtils.MIN_PITCH_ANGLE = -(Math.PI / 2);
CameraUtils.MAX_PITCH_ANGLE = (Math.PI / 2);
CameraUtils.MIN_DISTANCE = 0.001;
CameraUtils.MIN_LATITUDE = 0.001;
CameraUtils.MAX_LATITUDE = Math.PI;
CameraUtils.MIN_FOV = 30.0;
CameraUtils.MAX_FOV = 120.0;
CameraUtils.FOV_DELTA = 5.0;

CameraUtils.clamp = function(value, min, max) {
	return Math.min(Math.max(value, min), max);
}

CameraUtils.clampFOV = function(fov) {
	return CameraUtils.clamp(fov, CameraUtils.MIN_FOV, CameraUtils.MAX_FOV);
}

CameraUtils.clampLatitude = function(latitude) {
	return CameraUtils.clamp(latitude, CameraUtils.MIN_LATITUDE, CameraUtils.MAX_LATITUDE);
}

CameraUtils.degrees = function(radians)
{
    return radians * (180.0 / Math.PI);
};

CameraUtils.radians = function(degrees) {
    return degrees * (Math.PI / 180.0);
};

CameraUtils.getYawDirection = function(yaw)
{
    if (yaw >= -((Math.PI / 4) * 3) && yaw < -(Math.PI / 4) ) {
        return CameraDirection.EAST;
    }

    if (yaw >= -(Math.PI / 4)  && yaw < (Math.PI / 4) ) {
        return CameraDirection.SOUTH;
    }

    if (yaw >= (Math.PI / 4) && yaw < ((Math.PI / 4) * 3)) {
        return CameraDirection.WEST;
    }

    return CameraDirection.NORTH;
};

CameraUtils.normalizeAngle = function(angle)
{
    var normalized = angle;

    while (normalized <= -(Math.PI)) {
        normalized += (2 * Math.PI);
    }

    while (normalized > Math.PI) {
        normalized -= (2 * Math.PI);
    }

    return normalized;
};

CameraUtils.calculatePitch = function(position, lookAt)
{
    var dx = position[0] - lookAt[0];
    var dy = position[1] - lookAt[1];
    var dz = position[2] - lookAt[2];

    return -Math.atan2(dy, Math.sqrt(Math.pow(dx, 2) + Math.pow(dz, 2)));
};

CameraUtils.calculateYaw = function(position, lookAt)
{
    var dx = position[0] - lookAt[0];
    var dz = position[2] - lookAt[2];

    return (Math.atan2(dz, dx) - (Math.PI / 2));
};

CameraUtils.calculateDistance = function(position)
{
	var px = position[0];
	var py = position[1];
	var pz = position[2];
	
	return Math.sqrt(Math.pow(px, 2) + Math.pow(py, 2) + Math.pow(pz, 2));
};

CameraUtils.calculateLongitude = function(position)
{
    var px = position[0];
    var pz = -(position[2]);

    return Math.atan(pz / px);
};

CameraUtils.calculateLatitude = function(position)
{
    var py = position[1];
    var distance = CameraUtils.calculateDistance(position);

    return Math.acos(py / distance);
};

CameraUtils.calculatePosition = function(distance, latitude, longitude)
{
	var px = distance * Math.sin(latitude) * Math.cos(longitude);
    var py = distance * Math.cos(latitude);
	var pz = -(distance) * Math.sin(latitude) * Math.sin(longitude);

	return vec3.fromValues(px, py, pz);
};

var Camera = function(scale)
{
    this.freeScale = vec3.fromValues(scale, scale, scale);
    this.boundScale = scale;
    this.canvasHeight = gl.canvas.height;
    this.canvasWidth = gl.canvas.width;
    this.canvasRatio = this.canvasWidth / this.canvasHeight;
	
    this.type = CameraType.FREE;
    this.position = vec3.create();
    this.positionDelta = vec3.create();
    this.lookingAt = vec3.create();
    this.direction = vec3.create();
    this.up = vec3.fromValues(0.0, 1.0, 0.0);
    this.yaw = this.pitch = 0.0;
    this.yawDelta = this.pitchDelta = 0.0;
    this.distance = 0.0;
    this.distanceDelta = 0.0;
    this.latitude = this.longitude = 0.0;
    this.latitudeDelta = this.longitudeDelta = 0.0;
    this.fov = 0.0;

    this.projectionMatrix = mat4.create();
    this.viewMatrix = mat4.create();
    this.lightPosition = vec3.create();
    this.lightLocked = false;
};

Camera.prototype.setFOV = function(fov)
{
	this.fov = CameraUtils.clampFOV(fov);
    mat4.perspective(this.projectionMatrix, CameraUtils.radians(this.fov), this.canvasRatio, 0.01, 1000.0);
};

Camera.prototype.needsUpdate = function()
{
    return this.positionDelta[0] != 0.0 ||
           this.positionDelta[1] != 0.0 ||
           this.positionDelta[2] != 0.0 ||
           this.yawDelta != 0.0 		||
		   this.pitchDelta != 0.0		||
		   this.distanceDelta != 0.0	||
		   this.latitudeDelta != 0.0	||
		   this.longitudeDelta != 0.0;
};

Camera.prototype.update = function(forceUpdate)
{
    forceUpdate = (typeof forceUpdate === "undefined") ? false : forceUpdate;

    if (this.needsUpdate() || forceUpdate)
    {
    	vec3.subtract(this.direction, this.lookingAt, this.position);
		vec3.normalize(this.direction, this.direction);

		var axis = vec3.create();
		vec3.cross(axis, this.direction, this.up);
		vec3.normalize(axis, axis);
		
		var radPitchDelta = CameraUtils.radians(this.pitchDelta);
		var minPitchDelta = CameraUtils.MIN_PITCH_ANGLE - this.pitch;
		var maxPitchDelta = CameraUtils.MAX_PITCH_ANGLE - this.pitch;

		if (radPitchDelta > maxPitchDelta ||
            radPitchDelta < minPitchDelta) {
            radPitchDelta = 0.0;
        }

		var pitchQuat = quat.create();
		quat.setAxisAngle(pitchQuat, axis, radPitchDelta);

		var yawQuat = quat.create();
		quat.setAxisAngle(yawQuat, this.up, CameraUtils.radians(this.yawDelta));

		var rotation = quat.create();

		quat.multiply(rotation, yawQuat, pitchQuat);
		quat.normalize(rotation, rotation);
		vec3.transformQuat(this.direction, this.direction, rotation);
		
    	switch (this.type)
    	{
    	    case CameraType.FREE:
    	    {
				vec3.add(this.position, this.position, this.positionDelta);		
            	vec3.add(this.lookingAt, this.position, this.direction);
            	
				this.distance = CameraUtils.calculateDistance(this.position);
				this.longitude = CameraUtils.calculateLongitude(this.position);
                this.latitude = CameraUtils.calculateLatitude(this.position);
    	    } break;
    	    
    	    case CameraType.BOUND:
    	    {
    	    	this.distance += this.distanceDelta;
    	    	
    	    	if (this.distance < CameraUtils.MIN_DISTANCE) {
    	    		this.distance = CameraUtils.MIN_DISTANCE;	
    	    	}
    	    	
    	    	var longitude = this.longitude + this.longitudeDelta;
    	    	this.longitude = longitude;
    	    	
    	    	var latitude = this.latitude + this.latitudeDelta;
    	    	this.latitude = CameraUtils.clampLatitude(latitude);
    	    	
				this.position = CameraUtils.calculatePosition(this.distance, this.latitude, this.longitude);
				this.lookingAt[0] = this.lookingAt[1] = this.lookingAt[2] = 0.0;
    	    } break;
    	}
    	
   	
    	
    	this.pitch = CameraUtils.calculatePitch(this.position, this.lookingAt);
    	
    	var yaw = CameraUtils.calculateYaw(this.position, this.lookingAt);
		this.yaw = CameraUtils.normalizeAngle(yaw);

		this.positionDelta = vec3.create();
		this.pitchDelta = this.yawDelta = 0.0;
		this.distanceDelta = 0.0;
		this.latitudeDelta = this.longitudeDelta = 0.0;

		mat4.lookAt(this.viewMatrix, this.position, this.lookingAt, this.up);

		if (!this.lightLocked) {
		    vec3.copy(this.lightPosition, this.position);
		}
    }
};

Camera.prototype.move = function(direction)
{
    var temp = vec3.create();

    switch (direction)
    {
        case CameraMovement.UP:
        {
        	if (this.type == CameraType.FREE)
        	{
		        vec3.multiply(temp, this.up, this.freeScale);
		        vec3.add(this.positionDelta, this.positionDelta, temp);
	        } else {
	        	this.latitudeDelta -= CameraUtils.radians(this.boundScale);
	        }
        } break;

        case CameraMovement.DOWN:
        {
        	if (this.type == CameraType.FREE)
        	{
		        vec3.multiply(temp, this.up, this.freeScale);
		        vec3.subtract(this.positionDelta, this.positionDelta, temp);
	        } else {
	        	this.latitudeDelta += CameraUtils.radians(this.boundScale);
	        }            
        } break;

        case CameraMovement.LEFT:
        {
        	if (this.type == CameraType.FREE)
        	{
		        vec3.cross(temp, this.direction, this.up);
		        vec3.multiply(temp, temp, this.freeScale);
		        vec3.subtract(this.positionDelta, this.positionDelta, temp);
            } else {
            	this.longitudeDelta -= CameraUtils.radians(this.boundScale);
            }
        } break;

        case CameraMovement.RIGHT:
        {
        	if (this.type == CameraType.FREE)
        	{
		        vec3.cross(temp, this.direction, this.up);
		        vec3.multiply(temp, temp, this.freeScale);
		        vec3.add(this.positionDelta, this.positionDelta, temp);
	        } else {
	        	this.longitudeDelta += CameraUtils.radians(this.boundScale);
	        }
        } break;

        case CameraMovement.FORWARD:
        {
        	if (this.type == CameraType.FREE)
        	{
            	vec3.multiply(temp, this.direction, this.freeScale);
	            vec3.add(this.positionDelta, this.positionDelta, temp);
            } else {
            	this.distanceDelta -= this.boundScale;
            }
        } break;

        case CameraMovement.BACKWARD:
        {
        	if (this.type == CameraType.FREE)
        	{
	            vec3.multiply(temp, this.direction, this.freeScale);
            	vec3.subtract(this.positionDelta, this.positionDelta, temp);
            } else {
            	this.distanceDelta += this.boundScale;
            }
        } break;
    }
};

Camera.prototype.setPosition = function(position)
{
    vec3.copy(this.position, position);
    this.update(true);
};

Camera.prototype.changeYaw = function(degrees) {
    this.yawDelta += degrees;
};

Camera.prototype.changePitch = function(degrees) {
    this.pitchDelta += degrees;
};

Camera.prototype.rotate = function(deltaX, deltaY)
{
	if (this.type == CameraType.FREE)
	{
	    this.changeYaw(deltaX);
  	  	this.changePitch(deltaY);
  	}  	  
};
