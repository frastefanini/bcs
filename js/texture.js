var Texture = function(name, folder)
{
    this.name = name;
    this.folder = (typeof folder === "undefined") ? TEXTURE_ROOT_FOLDER : folder;
    this.texture = gl.createTexture();
    this.width = 0;
    this.height = 0;
};

Texture.prototype.load = function(minFilter, maxFilter)
{
    var $deferred = $.Deferred();
    var image = new Image();
    var texture = this;

    image.onload = function()
    {
        texture.width = image.width;
        texture.height = image.height;

        gl.bindTexture(gl.TEXTURE_2D, texture.texture);
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.REPEAT);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.REPEAT);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, minFilter);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, maxFilter);
        gl.generateMipmap(gl.TEXTURE_2D);
        gl.bindTexture(gl.TEXTURE_2D, null);

        $deferred.resolve();
    };

    image.src = TEXTURE_PATH + "/" + this.folder + "/" + this.name + ".png";
    return $deferred;
};