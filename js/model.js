/**
 * Bounding box
 */

var BB_LINE_INDICES =
    [
        0, 1, 1, 2, 2, 3, 3, 0,
        4, 5, 5, 6, 6, 7, 7, 4,
        0, 4, 1, 5, 2, 6, 3, 7
    ];

var BoundingBox = function()
{
    this.min = vec3.fromValues(Infinity, Infinity, Infinity);
    this.max = vec3.fromValues(-Infinity, -Infinity, -Infinity);
    this.center = vec3.create();
    this.loaded = false;

    this.glPoints = new Float32Array((8 + 1) * VEC3_LENGTH);
    this.glLineIndices = new Uint8Array(BB_LINE_INDICES);
};

BoundingBox.prototype.reset = function()
{
    this.min[0] = this.min[1] = this.min[2] = Infinity;
    this.max[0] = this.max[1] = this.max[2] = -Infinity;
    this.center[0] = this.center[1] = this.center[2] = 0.0;

    this.loaded = false;
};

BoundingBox.prototype.calculatePrimitives = function()
{
    var minX = this.min[0];
    var minY = this.min[1];
    var minZ = this.min[2];
    var maxX = this.max[0];
    var maxY = this.max[1];
    var maxZ = this.max[2];

    this.glPoints.set([minX, minY, maxZ], 0 * VEC3_LENGTH);
    this.glPoints.set([maxX, minY, maxZ], 1 * VEC3_LENGTH);
    this.glPoints.set(this.max, 2 * VEC3_LENGTH);
    this.glPoints.set([minX, maxY, maxZ], 3 * VEC3_LENGTH);
    this.glPoints.set(this.min, 4 * VEC3_LENGTH);
    this.glPoints.set([maxX, minY, minZ], 5 * VEC3_LENGTH);
    this.glPoints.set([maxX, maxY, minZ], 6 * VEC3_LENGTH);
    this.glPoints.set([minX, maxY, minZ], 7 * VEC3_LENGTH);

    this.glPoints.set(this.center, 8 * VEC3_LENGTH);
};

BoundingBox.prototype.fromPoints = function(points)
{
    for (var i = 0; i < points.length; i++)
    {
        var point = points[i];

        vec3.min(this.min, this.min, point);
        vec3.max(this.max, this.max, point);
    }

    this.center[0] = (this.min[0] + this.max[0]) / 2;
    this.center[1] = (this.min[1] + this.max[1]) / 2;
    this.center[2] = (this.min[2] + this.max[2]) / 2;

    this.calculatePrimitives();
    this.loaded = true;
};

BoundingBox.prototype.fromBoundingBoxes = function(boxes)
{
    for (var i = 0; i < boxes.length; i++)
    {
        var boundingBox = boxes[i];

        vec3.min(this.min, this.min, boundingBox.min);
        vec3.max(this.max, this.max, boundingBox.min);

        vec3.min(this.min, this.min, boundingBox.max);
        vec3.max(this.max, this.max, boundingBox.max);
    }

    this.center[0] = (this.min[0] + this.max[0]) / 2;
    this.center[1] = (this.min[1] + this.max[1]) / 2;
    this.center[2] = (this.min[2] + this.max[2]) / 2;

    this.calculatePrimitives();
    this.loaded = true;
};

/**
 * Surface
 */

var Surface = function(name, uDegree, vDegree, uKnots, vKnots, CP)
{
    this.name = name;
    this.uDegree = uDegree;
    this.vDegree = vDegree;
    this.uKnots = uKnots;
    this.vKnots = vKnots;
    this.CP = CP;
    this.normalVersus = null;
    this.trimCurves = null;
    this.isTrimmed = false;

    this.boundingBox = new BoundingBox();
    this.updated = false;
    this.canColorTick = true;

    this.hsvColor = Colors.randomHsv();
    this.hsvColor[1] = 0.75;
    this.hsvColor[2] = 0.75;
    this.rgbColor = vec3.create();

    this.rotateX = -(Math.PI / 2);
    this.tickCount = -1;

    this.points = [];
    this.glVertexData = null;
    this.glLineIndices = null;
    this.glTriangleIndices = null;
};

Surface.prototype.update = function(stacks, slices)
{
    var i;
    var nurbs;

    if (this.isTrimmed) {
        nurbs = Nurbs.evaluate_trim(this, stacks, slices);
    } else {
        nurbs = Nurbs.evaluate(this, stacks, slices);
    }

    this.points = nurbs.points;

    if (!this.boundingBox.loaded) {
        this.boundingBox.fromPoints(this.points);
    }

    var lines = nurbs.lines;
    var triangles = nurbs.triangles;
    var normals = nurbs.normals;
    var uvCoords = nurbs.uvCoords;

    this.glVertexData = new Float32Array(this.points.length * VERTEX_DATA_LENGTH);

    for (i = 0; i < this.points.length; i++)
    {
        this.glVertexData.set(this.points[i], i * VERTEX_DATA_LENGTH);
        this.glVertexData.set(normals[i], (i * VERTEX_DATA_LENGTH) + VEC3_LENGTH);
        this.glVertexData.set(uvCoords[i], (i * VERTEX_DATA_LENGTH) + (2 * VEC3_LENGTH));
    }

    this.glLineIndices = new Uint16Array(lines.length * SEGMENT_LENGTH);

    for (i = 0; i < lines.length; i++) {
        this.glLineIndices.set(lines[i].indices, i * SEGMENT_LENGTH);
    }

    this.glTriangleIndices = new Uint16Array(triangles.length * TRIANGLE_LENGTH);

    for (i = 0; i < triangles.length; i++)
    {
        this.glTriangleIndices.set(triangles[i].indices, i * TRIANGLE_LENGTH);
    }

    this.updated = true;
};

Surface.prototype.tickColors = function()
{
    this.hsvColor[0] = (this.hsvColor[0] + COLOR_INCREMENT) % 1.0;
    this.rgbColor = Colors.hsvToRgb(this.hsvColor);
};

Surface.prototype.tick = function()
{
    this.tickCount++;

    if (this.canColorTick) {
        this.tickColors();
    }
};

/**
 * Model
 */

var Model = function(name)
{
    this.name = name;
    this.surfaces = [];
    this.activeSurfaces = [];
    this.updated = false;
    this.canColorTick = true;

    this.hsvColor = Colors.randomHsv();
    this.hsvColor[1] = 0.75;
    this.hsvColor[2] = 0.75;
    this.rgbColor = vec3.create();

    this.rotateX = -(Math.PI / 2);
    this.tickCount = -1;

    this.glVertexData = null;
    this.glLineIndices = null;
    this.glTriangleIndices = null;
    this.modelMatrix = null;
    this.boundingBox = new BoundingBox();
};

Model.prototype.parse = function(src)
{
    var iges = Iges.parse(src);

    this.surfaces = iges.surfaces;
    this.activeSurfaces = iges.activeSurfaces;
};

Model.prototype.loadAndParse = function()
{
    var model = this;
    var fileName = this.name + "." + Iges.EXTENSION;
    var modelUrl = MODEL_PATH + "/" + fileName;

    return $.ajax(modelUrl).then(function(src) {
        model.parse(src);
    });
};

Model.prototype.updateBoundingBox = function()
{
    var model = this;
    var boundingBoxes = [];

    this.activeSurfaces.forEach(function(index)
    {
        var surface = model.surfaces[index];
        boundingBoxes.push(surface.boundingBox);
    });

    this.boundingBox.reset();
    this.boundingBox.fromBoundingBoxes(boundingBoxes);

    /* Model matrix */
    this.modelMatrix = mat4.create();

    var distance = vec3.create();
    vec3.negate(distance, this.boundingBox.center);

    mat4.rotateX(this.modelMatrix, this.modelMatrix, model.rotateX);
    mat4.translate(this.modelMatrix, this.modelMatrix, distance);
};

Model.prototype.update = function(stacks, slices)
{
    var model = this;
    var glVertexDataLength = 0;
    var glLineIndicesLength = 0;
    var glTriangleIndicesLength = 0;

    this.surfaces.forEach(function(surface)
    {
        surface.update(stacks, slices);

        glVertexDataLength += surface.glVertexData.length;
        glLineIndicesLength += surface.glLineIndices.length;
        glTriangleIndicesLength += surface.glTriangleIndices.length;
    });

    /* WebGL data */

    this.glVertexData = new Float32Array(glVertexDataLength);
    glVertexDataLength = 0;

    this.glLineIndices = new Uint32Array(glLineIndicesLength);
    glLineIndicesLength = 0;

    this.glTriangleIndices = new Uint32Array(glTriangleIndicesLength);
    glTriangleIndicesLength = 0;

    this.surfaces.forEach(function(surface)
    {
        var offset = glVertexDataLength / VERTEX_DATA_LENGTH;
        var glLineIndices = Utils.glShiftIndices(surface.glLineIndices, offset);
        var glTriangleIndices = Utils.glShiftIndices(surface.glTriangleIndices, offset);

        model.glVertexData.set(surface.glVertexData, glVertexDataLength);
        glVertexDataLength += surface.glVertexData.length;

        model.glLineIndices.set(glLineIndices, glLineIndicesLength);
        glLineIndicesLength += glLineIndices.length;

        model.glTriangleIndices.set(glTriangleIndices, glTriangleIndicesLength);
        glTriangleIndicesLength += glTriangleIndices.length;
    });

    this.updateBoundingBox();
    this.updated = true;
};

Model.prototype.tickColors = function()
{
    this.hsvColor[0] = (this.hsvColor[0] + COLOR_INCREMENT) % 1.0;
    this.rgbColor = Colors.hsvToRgb(this.hsvColor);
};

Model.prototype.tick = function()
{
    this.tickCount++;

    if (this.canColorTick) {
        this.tickColors();
    }

    this.surfaces.forEach(function(surface) {
        surface.tick();
    });
};
