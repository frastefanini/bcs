var WEBGL_NAMES =
[
    "webgl",
    "experimental-webgl",
    "webkit-3d",
    "moz-webgl"
];

var ANGLE = {
    NOT_IMPLEMENTED: -1, DISABLED: 0, ENABLED: 1
};

var WIN32 = "Win32";
var IE_RENDERER = "Internet Explorer";

var USER_AGENT = "User Agent: ";
var WEBGL_SUPPORT = "Supporto alla libreria WebGL: ";
var WEBGL_STATUS = "Stato della libreria WebGL: ";
var WEBGL_VERSION = "Versione di WebGL: ";
var ANGLE_STATUS = "Stato del layer di astrazione grafico (ANGLE): ";
var ENABLED_EXTS = "Numero di estensioni WebGL abilitate: ";
var UINT32_EXT = "OES_element_index_uint";
var UINT32_SUPPORT = "Supporto ai valori interi unsigned a 32 bit: ";

var SHADER_PATH = "assets/shaders";
var TEXTURE_PATH = "assets/textures";
var FONT_PATH = "assets/fonts";
var MODEL_PATH = "assets/models";

var TEXTURE_ROOT_FOLDER = ".";

var DEFAULT_ROTATION = 2.0;
var MOUSE_SCALE = .25;
var TICKS_PER_SECOND = 60;
var VEC2_LENGTH = 2;
var VEC3_LENGTH = 3;
var VERTEX_DATA_LENGTH = (2 * VEC3_LENGTH) + VEC2_LENGTH;
var FOV = 45.0;
var COLOR_INCREMENT = 0.001;
var MAX_UNPROCESSED_TICKS = 10.0;
var CAMERA_SCALE = 0.5;
var MIN_LOD = 1;
var MAX_LOD = 16;
var LOD_MULTIPLIER = 8;

var SURFACE_ROW =
"<tr>\
    <td>%s</td>\
    <td>\
        <div class=\"slider\">\
            <input type=\"checkbox\" value=\"%d\" id=\"surface%d\" name=\"check\" onchange=\"onSliderClick()\" checked />\
            <label for=\"surface%d\"></label>\
        </div>\
    </td>\
</tr>";

if (typeof String.prototype.startsWith !== "function")
{
    String.prototype.startsWith = function(str) {
        return this.slice(0, str.length) == str;
    };
}

if (typeof String.prototype.endsWith !== "function")
{
    String.prototype.endsWith = function(str) {
        return this.slice(-str.length) == str;
    };
}

var Utils = {};

Utils.glShiftIndices = function(indices, offset)
{
    var shifted = new Uint32Array(indices.length);
    var i = indices.length;

    while (i--) {
        shifted[i] = offset + indices[i];
    }

    return shifted;
};