var Shader = function(name)
{
    this.name = name;
    this.program = null;
    this.uniforms = {};
    this.attrs = {};
};

Shader.prototype.compile = function(
    source, shaderType)
{
    var shader = gl.createShader(shaderType);

    gl.shaderSource(shader, source);
    gl.compileShader(shader);

    if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS))
    {
        console.log(gl.getShaderInfoLog(shader));
        return null;
    }

    return shader;
};

Shader.prototype.link = function(
    vertexShader, fragmentShader)
{
    var program = gl.createProgram();

    gl.attachShader(program, vertexShader);
    gl.attachShader(program, fragmentShader);
    gl.linkProgram(program);

    if (!gl.getProgramParameter(program, gl.LINK_STATUS))
    {
        console.log(gl.getProgramInfoLog(program));
        return null;
    }

    return program;
};

Shader.prototype.use = function() {
    gl.useProgram(this.program);
};

Shader.prototype.getShaderUniforms = function()
{
    var uniformCount = gl.getProgramParameter(this.program, gl.ACTIVE_UNIFORMS);

    for (var index = 0; index < uniformCount; index++)
    {
        var name = gl.getActiveUniform(this.program, index).name;
        this.uniforms[name] = gl.getUniformLocation(this.program, name);
    }
};

Shader.prototype.getShaderAttributes = function()
{
    var attributeCount = gl.getProgramParameter(this.program, gl.ACTIVE_ATTRIBUTES);

    for (var index = 0; index < attributeCount; index++)
    {
        var name = gl.getActiveAttrib(this.program, index).name;
        this.attrs[name] = gl.getAttribLocation(this.program, name);
    }
};

Shader.prototype.create = function(
    vertexShaderSrc, fragmentShaderSrc)
{
    var vertexShader = this.compile(vertexShaderSrc, gl.VERTEX_SHADER);
    var fragmentShader = this.compile(fragmentShaderSrc, gl.FRAGMENT_SHADER);
    this.program = this.link(vertexShader, fragmentShader);

    this.use();

    this.getShaderUniforms();
    this.getShaderAttributes();
};

Shader.prototype.loadAndCompile = function()
{
    var shaderRootUrl = SHADER_PATH + "/" + this.name;
    var shader = this;

    return $.when
    (
        $.ajax(shaderRootUrl + ".vertex.glsl"),
        $.ajax(shaderRootUrl + ".fragment.glsl")
    ).then(function(vertexShader, fragmentShader)
    {
        var vertexShaderSrc = vertexShader[0];
        var fragmentShaderSrc = fragmentShader[0];

        shader.create(vertexShaderSrc, fragmentShaderSrc);
    });
};

Shader.prototype.vertexAttribPointer = function(
    name, size, floatsPerVertex, offset)
{
    if (!this.attrs.hasOwnProperty(name)) {
        return;
    }

    gl.enableVertexAttribArray(this.attrs[name]);
    gl.vertexAttribPointer(this.attrs[name], size, gl.FLOAT, false, floatsPerVertex * Float32Array.BYTES_PER_ELEMENT, offset * Float32Array.BYTES_PER_ELEMENT);
};

Shader.prototype.uniformMatrix4fv = function(
    name, transpose, matrix)
{
    if (!this.uniforms.hasOwnProperty(name)) {
        return;
    }

    gl.uniformMatrix4fv(this.uniforms[name], transpose, matrix);
};

Shader.prototype.uniformMatrix3fv = function(
    name, transpose, matrix)
{
    if (!this.uniforms.hasOwnProperty(name)) {
        return;
    }

    gl.uniformMatrix3fv(this.uniforms[name], transpose, matrix);
};

Shader.prototype.uniform3fv = function(name, vector)
{
    if (!this.uniforms.hasOwnProperty(name)) {
        return;
    }

    gl.uniform3fv(this.uniforms[name], vector);
};

Shader.prototype.uniform4fv = function(name, vector)
{
    if (!this.uniforms.hasOwnProperty(name)) {
        return;
    }

    gl.uniform4fv(this.uniforms[name], vector);
};