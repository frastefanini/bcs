precision mediump float;

uniform vec3 uColor;

void main(void)
{
    if (gl_FrontFacing) {
        gl_FragColor = vec4(uColor, 1.0);
    } else {
        gl_FragColor = vec4(uColor, 1.0);
    }
}