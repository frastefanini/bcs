precision mediump float;

const vec3 La = vec3(0.4);
const vec3 Ld = vec3(1.0);
const vec3 Ls = vec3(1.0);
const vec3 Ks = vec3(0.8);
const float f = 100.0;          // Shininess

attribute vec3 aPosition;       // Vertex position
attribute vec3 aNormal;         // Vertex normal vector

struct GLMatrix {
    mat4 p;         // Projection
    mat4 mv;        // ModelView
    mat3 n;         // Normal
};

uniform GLMatrix uMatrix;
uniform vec3 uLightPosition;     // Light position in eye coordinates
uniform vec3 uColor;             // Both ambient and diffuse color (ambient/diffuse reflectivity)

varying vec3 vFrontColor;
varying vec3 vBackColor;

vec3 phongModel(vec3 position, vec3 normal)
{
    vec3 s = normalize(uLightPosition - position);
    vec3 v = normalize(-position);
    vec3 n = normalize(normal);
    vec3 r = reflect(-s, n);
    vec3 Ka = uColor;
    vec3 Kd = uColor;

    vec3 ambient = La * Ka;
    vec3 diffuse = Ld * Kd * max(0.0, dot(s, n));
    vec3 specular = Ls * Ks * pow(max(0.0, dot(r, v)), f);

    return (ambient + diffuse + specular);
}

void main(void)
{
    vec3 position = vec3(uMatrix.mv * vec4(aPosition, 1.0));
    vec3 normal = normalize(uMatrix.n * aNormal);

    vFrontColor = phongModel(position, normal);
    vBackColor = phongModel(position, -normal);
    gl_Position = uMatrix.p * vec4(position, 1.0);
}