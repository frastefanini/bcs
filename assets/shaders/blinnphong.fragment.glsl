precision mediump float;

const vec3 La = vec3(0.4);
const vec3 Ld = vec3(1.0);
const vec3 Ls = vec3(1.0);
const vec3 Ks = vec3(0.8);
const float f = 100.0;           // Shininess

uniform vec3 uLightPosition;     // Light position in eye coordinates
uniform vec3 uColor;             // Both ambient and diffuse color (ambient/diffuse reflectivity)

varying vec3 vPosition;
varying vec3 vNormal;

vec3 blinnPhongModel(vec3 position, vec3 normal)
{
    vec3 s = normalize(uLightPosition - position);
    vec3 v = normalize(-position);
    vec3 n = normalize(normal);
    vec3 h = normalize(v + s);
    vec3 Ka = uColor;
    vec3 Kd = uColor;

    vec3 ambient = La * Ka;
    vec3 diffuse = Ld * Kd * max(0.0, dot(s, n));
    vec3 specular = Ls * Ks * pow(max(0.0, dot(h, n)), f);

    return (ambient + diffuse + specular);
}

void main(void)
{
    if (gl_FrontFacing) {
        gl_FragColor = vec4(blinnPhongModel(vPosition, vNormal), 1.0);
    } else {
        gl_FragColor = vec4(blinnPhongModel(vPosition, -vNormal), 1.0);
    }
}