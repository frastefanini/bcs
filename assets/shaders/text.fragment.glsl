precision mediump float;

varying vec2 vTextureCoord;

uniform sampler2D uTexture;
uniform vec4 uColor;

void main(void)
{
  vec4 col = texture2D(uTexture, vTextureCoord);

  if (col.a > 0.0)
  {
    vec3 color = col.rgb * uColor.rbg;
    gl_FragColor = vec4(color, uColor.a * col.a);
  } else {
    discard;
  }
}