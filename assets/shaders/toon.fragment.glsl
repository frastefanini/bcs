precision mediump float;

const vec3 La = vec3(0.6);
const vec3 Ld = vec3(1.0);
const vec3 Ls = vec3(1.0);
const int levels = 4;
float scaleFactor = 1.0 / float(levels);

uniform vec3 uLightPosition;     // Light position in eye coordinates
uniform vec3 uColor;             // Both ambient and diffuse color (ambient/diffuse reflectivity)

varying vec3 vPosition;
varying vec3 vNormal;

vec3 toonModel(vec3 position, vec3 normal)
{
    vec3 s = normalize(uLightPosition - position);
    vec3 n = normalize(normal);
    float cosine = max(0.0, dot(s, n));
    vec3 Ka = uColor;
    vec3 Kd = uColor;

    vec3 ambient = La * Ka;
    vec3 diffuse = Ld * Kd * floor(cosine * float(levels)) * scaleFactor;

    return (ambient + diffuse);
}

void main(void)
{
    if (gl_FrontFacing) {
        gl_FragColor = vec4(toonModel(vPosition, vNormal), 1.0);
    } else {
        gl_FragColor = vec4(toonModel(vPosition, -vNormal), 1.0);
    }
}