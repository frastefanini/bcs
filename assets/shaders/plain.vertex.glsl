precision mediump float;

const float pointSize = 4.0;
attribute vec3 aPosition;       // Vertex position

struct GLMatrix {
    mat4 p;         // Projection
    mat4 mv;        // ModelView
    mat3 n;         // Normal
};

uniform GLMatrix uMatrix;

void main(void)
{
    gl_PointSize = pointSize;
    gl_Position = uMatrix.p * uMatrix.mv * vec4(aPosition, 1.0);
}