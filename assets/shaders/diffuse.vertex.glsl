precision mediump float;

const vec3 Ld = vec3(1.0);      // Diffuse light intensity

attribute vec3 aPosition;       // Vertex position
attribute vec3 aNormal;         // Vertex normal vector

struct GLMatrix {
    mat4 p;         // Projection
    mat4 mv;        // ModelView
    mat3 n;         // Normal
};

uniform GLMatrix uMatrix;
uniform vec3 uLightPosition;     // Light position in eye coordinates
uniform vec3 uColor;             // Color (diffuse reflectivity)

varying vec3 vFrontColor;
varying vec3 vBackColor;

vec3 diffuseModel(vec3 position, vec3 normal)
{
    vec3 s = normalize(uLightPosition - position);
    vec3 n = normal;
    vec3 Kd = uColor;

    return Ld * Kd * max(0.0, dot(s, n));
}

void main(void)
{
    vec3 position = vec3(uMatrix.mv * vec4(aPosition, 1.0));
    vec3 normal = normalize(uMatrix.n * aNormal);

    vFrontColor = diffuseModel(position, normal);
    vBackColor = diffuseModel(position, -normal);
    gl_Position = uMatrix.p * vec4(position, 1.0);
}