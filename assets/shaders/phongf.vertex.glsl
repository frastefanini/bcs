precision mediump float;

attribute vec3 aPosition;       // Vertex position
attribute vec3 aNormal;         // Vertex normal vector

struct GLMatrix {
    mat4 p;         // Projection
    mat4 mv;        // ModelView
    mat3 n;         // Normal
};

uniform GLMatrix uMatrix;

varying vec3 vPosition;
varying vec3 vNormal;

void main(void)
{
    vPosition = vec3(uMatrix.mv * vec4(aPosition, 1.0));
    vNormal = normalize(uMatrix.n * aNormal);

    gl_Position = uMatrix.p * vec4(vPosition, 1.0);
}