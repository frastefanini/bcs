precision mediump float;

attribute vec3 aPosition;

uniform mat4 uModelMatrix;
uniform mat4 uViewMatrix;
uniform mat4 uOrthoMatrix;
uniform mat4 uTextureMatrix;

varying vec2 vTextureCoord;

void main() {

  gl_Position = uOrthoMatrix * uViewMatrix * uModelMatrix * vec4(aPosition, 1.0);
  vTextureCoord = (uTextureMatrix * vec4(aPosition, 1.0)).xy;

}