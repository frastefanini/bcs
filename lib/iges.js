var Iges = {};

Iges.EXTENSION = "igs";
Iges.FIELD_DELIMITER = ',';
Iges.SECTION = 72;
Iges.SECTION_PD = 'P';
Iges.B_SPLINE_SURFACE = 128;

Iges.parseFloat = function(str)
{
    return parseFloat(str.replace(/D/g, "e"));
};

Iges.parseBSplineSurface = function(fields, id)
{
    var i;
    var index = 1;

    var vCPCount = 1 + parseInt(fields[index++]);
    var uCPCount = 1 + parseInt(fields[index++]);
    var vDegree = parseInt(fields[index++]);
    var uDegree = parseInt(fields[index++]);

    index += 5;

    var vKnotsCount = vDegree + vCPCount + 1;
    var uKnotsCount = uDegree + uCPCount + 1;
    var weightsCount = vCPCount * uCPCount;

    var vKnots = [];
    vKnots[0] = 0.0;

    for (i = 0; i < vKnotsCount; i++) {
        vKnots[i + 1] = Iges.parseFloat(fields[index++]);
    }

    var uKnots = [];
    uKnots[0] = 0.0;

    for (i = 0; i < uKnotsCount; i++) {
        uKnots[i + 1] = Iges.parseFloat(fields[index++]);
    }

    var weights = [];

    for (i = 0; i < weightsCount; i++) {
        weights[i] = Iges.parseFloat(fields[index++]);
    }

    var CP = [];

    for (i = 0; i < uCPCount; i++)
    {
        CP[i] = [];

        for (var j = 0; j < vCPCount; j++)
        {
            var x = Iges.parseFloat(fields[index++]);
            var y = Iges.parseFloat(fields[index++]);
            var z = Iges.parseFloat(fields[index++]);
            var w = weights[(i * vCPCount) + j];

            CP[i][j] = vec4.fromValues(x, y, z, w);
        }
    }

    var name = "Surface #" + id;
    return new Surface(name, uDegree, vDegree, uKnots, vKnots, CP);
};

Iges.parse = function(source)
{
    var lines = source.split('\u000A');
    var currentSection = '';
    var data = "";
    var surfaces = [];
    var activeSurfaces = [];

    for (var i = 0; i < lines.length; i++)
    {
        var line = lines[i];
        var section = line[Iges.SECTION];

        if (section == currentSection)
        {
            data += line.substr(0, 72);
            continue;
        }

        if (currentSection == Iges.SECTION_PD)
        {
            var records = [];
            var j;

            for (j = 0; j < data.length; j += Iges.SECTION)
            {
                var record = data.substr(j, 64).replace(/[; ]*$/, "");
                var index = Math.floor(parseInt(data.substr(j + 64, 8)) / 2);

                if (records[index] === undefined) {
                    records[index] = "";
                }

                records[index] += record;
            }

            var count = 0;

            for (j = 0; j < records.length; j++)
            {
                var fields = records[j].split(Iges.FIELD_DELIMITER);

                if (fields[0] == Iges.B_SPLINE_SURFACE)
                {
                    surfaces[count] = Iges.parseBSplineSurface(fields, count);
                    activeSurfaces.push(count);

                    count++;
                }
            }
        }

        data = line.substr(0, Iges.SECTION);
        currentSection = section;
    }

    var result = {}

    result.surfaces = surfaces;
    result.activeSurfaces = activeSurfaces;

    return result;
};