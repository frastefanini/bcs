/* NURBS utilities */

var NurbsUtils = {};

NurbsUtils.precisionPoints = 4;
NurbsUtils.precision = Math.pow(10, NurbsUtils.precisionPoints);

NurbsUtils.computeKey = function(point)
{
    return  Math.round(point[0] * NurbsUtils.precision) + '_' +
        Math.round(point[1] * NurbsUtils.precision) + '_' +
        Math.round(point[2] * NurbsUtils.precision);
};

NurbsUtils.getUniqueKey = function(index, points, unique)
{
    var point = points[index];
    var key = NurbsUtils.computeKey(point);

    if (unique[key] !== undefined) {
        return (unique[key] != index) ? unique[key] : index;
    }

    return index;
};

NurbsUtils.calculateUV = function(x, y, a, b)
{
    var uo = 1 / a;
    var vo = 1 / b;
    var u = -1.0 + (x * (2 * uo));
    var v = -1.0 + (y * (2 * vo));

    return vec2.fromValues(1.0 - Math.abs(u), 1.0 - Math.abs(v));
};

/* NURBS (from XCModel) */

var Nurbs = {};

Nurbs.model_sur = function(surface, m1, m2)
{
    var i, j;
    var kumin = surface.uDegree + 1;
    var kumax = (surface.uKnots.length - 1) - surface.uDegree;
    var hu = [];

    var h1 = (surface.uKnots[kumax] - surface.uKnots[kumin]) / m1;
    var h = 0.0;
    m1 = 0;

    for (i = kumin; i < kumax; i++)
    {
        if (surface.uKnots[i] != surface.uKnots[i + 1])
        {
            hu[m1] = surface.uKnots[i];
            m1++;

            var np1 = Math.ceil((surface.uKnots[i + 1] - surface.uKnots[i]) / h1);

            if (np1 != 0) {
                h = (surface.uKnots[i + 1] - surface.uKnots[i]) / np1;
            }

            for (j = 1; j < np1; j++)
            {
                hu[m1] = surface.uKnots[i] + (j * h);
                m1++;
            }
        }
    }

    hu[m1] = surface.uKnots[kumax];

    var kvmin = surface.vDegree + 1;
    var kvmax = (surface.vKnots.length - 1) - surface.vDegree;
    var hv = [];

    if (surface.vKnots.length > 1)
    {
        var h2 = (surface.vKnots[kvmax] - surface.vKnots[kvmin]) / m2;
        m2 = 0;

        for (i = kvmin; i < kvmax; i++)
        {
            if (surface.vKnots[i] != surface.vKnots[i + 1])
            {
                hv[m2] = surface.vKnots[i];
                m2++;

                var np2 = Math.ceil((surface.vKnots[i + 1] - surface.vKnots[i]) / h2);

                if (np2 != 0) {
                    h = (surface.vKnots[i + 1] - surface.vKnots[i]) / np2;
                }

                for (j = 1; j < np2; j++)
                {
                    hv[m2] = surface.vKnots[i] + (j * h);
                    m2++;
                }
            }
        }

        hv[m2] = surface.vKnots[kvmax];
    }

    return Nurbs.compute(surface, hu, hv, m1, m2);
};

Nurbs.compute = function(surface, hu, hv, m1, m2)
{
    var i, k1, lk;
    var bs0 = [];
    var bs1 = [[]];
    var lb1 = [];
    var kb1 = [];
    var a = [];
    var l = 0;
    var k = surface.uDegree + 1;

    for (k1 = 0; k1 <= m1; k1++)
    {
        lk = Nurbs.b_spline_u(surface, hu[k1], l, k, bs0);

        l = lk.l;
        k = lk.k;
        bs0 = lk.bs;

        var j = 0;

        bs1[k1] = [];

        for (i = l; i <= k; i++) {

            j++;

            bs1[k1][j] = bs0[i];
            lb1[k1] = l;
            kb1[k1] = k;

        }
    }

    k = surface.vDegree + 1;
    var contrq = [];

    for (var k2 = 0; k2 <= m2; k2++) {

        if (surface.vKnots.length > 1) {

            lk = Nurbs.b_spline_v(surface, hv[k2], l, k, bs0);

            l = lk.l;
            k = lk.k;
            bs0 = lk.bs;

        } else {

            l = k = 1;
            bs0[1] = 1.0;

        }

        for (i = 0; i < surface.CP.length; i++) {
            contrq[i] = Nurbs.calcolo1(bs0, surface, i, l, k);
        }

        a[k2] = [];

        for (k1 = 0; k1 <= m1; k1++) {
            a[k2][k1] = Nurbs.calcolo2(bs1, k1, contrq, lb1[k1], kb1[k1]);
        }

    }

    var ret = {};

    ret.a = a;
    ret.m1 = m1;
    ret.m2 = m2;
    ret.hu = hu;
    ret.hv = hv;

    return ret;
};

Nurbs.b_spline = function(t, u, g, ll, kk, nt, bs)
{
    var j;
    var mid = 0;
    var dt = 0.0;
    var v = 0.0;

    if (kk == (g + 1))
    {
        ll = g + 1;
        kk = nt - g;

        while ((kk - ll) > 1)
        {
            mid = Math.ceil((kk + ll) / 2);

            if (u < t[mid]) {
                kk = mid;
            } else {
                ll = mid;
            }
        }
    } else {
        while ((t[kk] <= u) && (kk < (nt - g))) {
            kk++;
        }

        ll = kk - 1;
    }

    if (u == t[ll]) {
        kk = ll;
    }

    for (j = (ll - g); j <= kk; j++) {
        bs[j] = 0.0;
    }

    bs[ll] = 1.0 / (t[ll + 1] - t[ll]);
    ll--;
    kk--;

    for (var i = 2; i <= g; i++)
    {
        for (j = ll; j <= kk; j++)
        {
            dt = t[j + i] - t[j];
            v = (u - t[j]) / dt;
            bs[j] = (v * bs[j]) + ((1.0 - v) * bs[j + 1]);
        }

        bs[kk + 1] = 0.0;
        ll--;
    }

    for (j = ll; j <= kk; j++) {
        bs[j] = ((u - t[j]) * bs[j]) + ((t[j + g + 1] - u) * bs[j + 1]);
    }

    var lk = {}

    lk.l = ll;
    lk.k = kk;
    lk.bs = bs;

    return lk;
};

Nurbs.b_spline_u = function(surface, u, ll, kk, bs)
{
    var t = surface.uKnots;
    var g = surface.uDegree;
    var nt = surface.uKnots.length - 1;

    return Nurbs.b_spline(t, u, g, ll, kk, nt, bs);
};

Nurbs.b_spline_v = function(surface, u, ll, kk, bs)
{
    var t = surface.vKnots;
    var g = surface.vDegree;
    var nt = surface.vKnots.length - 1;

    return Nurbs.b_spline(t, u, g, ll, kk, nt, bs);
};

Nurbs.calcolo1 = function(bs, surface, t, l, k)
{
    var j;
    var xx = 0.0;
    var yy = 0.0;
    var zz = 0.0;
    var ww = 0.0;

    for (var i = l; i <= k; i++)
    {
        j = i - 1;
        var cp = surface.CP[t][j];

        xx += (cp[3] * cp[0] * bs[i]);
        yy += (cp[3] * cp[1] * bs[i]);
        zz += (cp[3] * cp[2] * bs[i]);
        ww += (cp[3] * bs[i]);
    }

    var ret = vec4.fromValues(xx, yy, zz, ww);
    return ret;
};

Nurbs.calcolo2 = function(bs1, q, p, l, k)
{
    var xx = 0.0;
    var yy = 0.0;
    var zz = 0.0;
    var w = 0.0;

    var j = 0;

    for (var i = l; i <= k; i++)
    {
        ++j;
        var jj = i - 1;

        xx += (p[jj][0] * bs1[q][j]);
        yy += (p[jj][1] * bs1[q][j]);
        zz += (p[jj][2] * bs1[q][j]);
        w += (p[jj][3] * bs1[q][j]);
    }

    xx /= w;
    yy /= w;
    zz /= w;

    var ret = vec3.fromValues(xx, yy, zz);
    return ret;
};

Nurbs.evaluate = function(surface, stacks, slices)
{
    var j, k, a, b, c, d, aa, bb, cc, dd, uva, uvb, uvc, uvd;
    var segment, triangle;
    var surfp = Nurbs.model_sur(surface, stacks, slices);
    var m = surfp.m1;
    var n = surfp.m2;

    var nPoints = 0;
    var nLines = 0;
    var nTriangles = 0;

    var points = [];
    var normals = [];
    var uvCoords = [];
    var lines = [];
    var triangles = [];

    var unique = {};

    for (j = 0; j <= n; j++)
    {
        for (k = 0; k <= m; k++)
        {
            var point = surfp.a[j][k];
            var key = NurbsUtils.computeKey(point);

            if (unique[key] === undefined) {
                unique[key] = nPoints;
            }

            points[nPoints] = point;
            normals[nPoints] = vec3.create();
            uvCoords[nPoints] = NurbsUtils.calculateUV(k, j, m, n);

            nPoints++;
        }
    }

    for (j = 0; j <= n; j++) {
        for (k = 0; k < m; k++)
        {
            aa = (j * (m + 1)) + k;
            bb = aa + 1;

            a = NurbsUtils.getUniqueKey(aa, points, unique);
            b = NurbsUtils.getUniqueKey(bb, points, unique);

            segment = new Segment(a, b);

            if (!segment.isDegenerate(points))
            {
                lines[nLines] = segment;
                nLines++;
            }
        }
    }

    for (j = 0; j < n; j++) {
        for (k = 0; k <= m; k++)
        {
            aa = (j * (m + 1)) + k;
            bb = ((j + 1) * (m + 1)) + k;

            a = NurbsUtils.getUniqueKey(aa, points, unique);
            b = NurbsUtils.getUniqueKey(bb, points, unique);

            segment = new Segment(a, b);

            if (!segment.isDegenerate(points))
            {
                lines[nLines] = segment;
                nLines++;
            }
        }
    }

    for (j = 0; j < n; j++) {
        for (k = 0; k < m; k++)
        {
            aa = (j * (m + 1)) + k;
            bb = aa + 1;
            dd = ((j + 1) * (m + 1)) + k;
            cc = dd + 1;

            a = NurbsUtils.getUniqueKey(aa, points, unique);
            b = NurbsUtils.getUniqueKey(bb, points, unique);
            d = NurbsUtils.getUniqueKey(dd, points, unique);
            c = NurbsUtils.getUniqueKey(cc, points, unique);

            triangle = new Triangle(a, b, d)

            if (!triangle.isDegenerate(points))
            {
                triangle.calculateNormal(points);

                triangles[nTriangles] = triangle;
                nTriangles++;
            }

            triangle =  new Triangle(b, c, d);

            if (!triangle.isDegenerate(points))
            {
                triangle.calculateNormal(points);

                triangles[nTriangles] = triangle;
                nTriangles++;
            }
        }
    }

    for (j = 0; j < triangles.length; j++)
    {
        triangle = triangles[j];

        vec3.add(normals[triangle.a], normals[triangle.a], triangle.normal);
        vec3.add(normals[triangle.b], normals[triangle.b], triangle.normal);
        vec3.add(normals[triangle.c], normals[triangle.c], triangle.normal);
    }

    for (j = 0; j < points.length; j++) {
        vec3.normalize(normals[j], normals[j]);
    }

    var result = {};

    result.points = points;
    result.lines = lines;
    result.triangles = triangles;
    result.normals = normals;
    result.uvCoords = uvCoords;

    return result;
};

Nurbs.evaluate_trim = function(surface, stacks, slices)
{
    var q, j, i, k, ip1, jp1, a, b, c, d, aa, bb, cc, dd;
    var segment, triangle;
    var surfp = Nurbs.model_sur(surface, stacks, slices);
    var m = surfp.m1;
    var n = surfp.m2;
    var hu = surfp.hu;
    var hv = surfp.hv;

    var nPoints = 0;
    var nLines = 0;
    var nTriangles = 0;

    var points = [];
    var normals = [];
    var uvCoords = [];
    var lines = [];
    var triangles = [];

    var tLines = [];
    var tTriangles = [];

    var unique = {};

    for (i = 0; i <= n; i++)
    {
        tLines[i] = vec3.fromValues(0.0, 0.0, -1.0);
        tTriangles[i] = vec3.fromValues(0.0, -1.0, 0.0);

        for (q = 0; q < surface.trimCurves.length; q++)
        {
            a = surface.trimCurves[q][0][0];
            b = surface.trimCurves[q][0][1];
            c = surface.trimCurves[q][1][0];
            d = surface.trimCurves[q][2][1];

            if ((a < hu[0]) && (hu[0] < b) &&
                (c < hv[i - 1]) && (hv[i - 1] < d))
            {
                tTriangles[i][1] = 0.0;
            }
        }
    }

    nPoints = 0;

    for (j = 0; j < m; j++)
    {
        jp1 = j + 1;

        for (i = 0; i <= n; i++)
        {
            tTriangles[i][0] = tTriangles[i][1];
            tTriangles[i][1] = -1.0;

            tLines[i][0] = tLines[i][2];
            tLines[i][1] = -1.0;
            tLines[i][2] = -1.0;

            for (q = 0; q < surface.trimCurves.length; q++)
            {
                a = surface.trimCurves[q][0][0];
                b = surface.trimCurves[q][0][1];
                c = surface.trimCurves[q][1][0];
                d = surface.trimCurves[q][2][1];

                if ((a < hu[j]) && (hu[j] < b) &&
                    (c < hv[i - 1]) && (hv[i - 1] < d))
                {
                    tTriangles[i][1] = 0.0;
                }
            }
        }

        for (i = 0; i < n; i++)
        {
            ip1 = i + 1;

            if ((tTriangles[i][0]) && (tTriangles[ip1][0]) &&
                (tTriangles[i][1]) && (tTriangles[ip1][1]))
            {
                var point, key;

                if (tTriangles[i][0] == -1.0)
                {
                    point = surfp.a[i][j];
                    key = NurbsUtils.computeKey(point);

                    if (unique[key] === undefined) {
                        unique[key] = nPoints;
                    }

                    points[nPoints] = point;
                    normals[nPoints] = vec3.create();
                    uvCoords[nPoints] = NurbsUtils.calculateUV(j, i, m, n);

                    tTriangles[i][0] = nPoints;
                    nPoints++;
                }

                if (tTriangles[ip1][0] == -1.0)
                {
                    point = surfp.a[ip1][j];
                    key = NurbsUtils.computeKey(point);

                    if (unique[key] === undefined) {
                        unique[key] = nPoints;
                    }

                    points[nPoints] = point;
                    normals[nPoints] = vec3.create();
                    uvCoords[nPoints] = NurbsUtils.calculateUV(j, ip1, m, n);

                    tTriangles[ip1][0] = nPoints;
                    nPoints++;
                }

                if (tTriangles[i][1] == -1.0)
                {
                    point = surfp.a[i][jp1];
                    key = NurbsUtils.computeKey(point);

                    if (unique[key] === undefined) {
                        unique[key] = nPoints;
                    }

                    points[nPoints] = point;
                    normals[nPoints] = vec3.create();
                    uvCoords[nPoints] = NurbsUtils.calculateUV(jp1, i, m, n);

                    tTriangles[i][1] = nPoints;
                    nPoints++;
                }

                if (tTriangles[ip1][1] == -1.0)
                {
                    point = surfp.a[ip1][jp1];
                    key = NurbsUtils.computeKey(point);

                    if (unique[key] === undefined) {
                        unique[key] = nPoints;
                    }

                    points[nPoints] = point;
                    normals[nPoints] = vec3.create();
                    uvCoords[nPoints] = NurbsUtils.calculateUV(jp1, ip1, m, n);

                    tTriangles[ip1][1] = nPoints;
                    nPoints++;
                }

                if (tLines[i][0] == -1.0)
                {
                    aa = tTriangles[i][0];
                    bb = tTriangles[ip1][0];

                    a = NurbsUtils.getUniqueKey(aa, points, unique);
                    b = NurbsUtils.getUniqueKey(bb, points, unique);

                    segment = new Segment(a, b);

                    if (!segment.isDegenerate(points))
                    {
                        lines[nLines] = segment;
                        nLines++;
                    }

                    tLines[i][0] = 0.0;
                }

                if (tLines[i][1] == -1.0)
                {
                    aa = tTriangles[i][0];
                    bb = tTriangles[i][1];

                    a = NurbsUtils.getUniqueKey(aa, points, unique);
                    b = NurbsUtils.getUniqueKey(bb, points, unique);

                    segment = new Segment(a, b);

                    if (!segment.isDegenerate(points))
                    {
                        lines[nLines] = segment;
                        nLines++;
                    }

                    tLines[i][1] = 0.0;
                }

                if (tLines[i][2] == -1.0)
                {
                    aa = tTriangles[i][1];
                    bb = tTriangles[ip1][1];

                    a = NurbsUtils.getUniqueKey(aa, points, unique);
                    b = NurbsUtils.getUniqueKey(bb, points, unique);

                    segment = new Segment(a, b);

                    if (!segment.isDegenerate(points))
                    {
                        lines[nLines] = segment;
                        nLines++;
                    }

                    tLines[i][2] = 0.0;
                }

                if (tLines[ip1][1] == -1.0)
                {
                    aa = tTriangles[ip1][0];
                    bb = tTriangles[ip1][1];

                    a = NurbsUtils.getUniqueKey(aa, points, unique);
                    b = NurbsUtils.getUniqueKey(bb, points, unique);

                    segment = new Segment(a, b);

                    if (!segment.isDegenerate(points))
                    {
                        lines[nLines] = segment;
                        nLines++;
                    }

                    tLines[ip1][1] = 0.0;
                }

                aa = tTriangles[i][1];
                bb = tTriangles[i][0];
                cc = tTriangles[ip1][0];
                dd = tTriangles[ip1][1];

                a = NurbsUtils.getUniqueKey(aa, points, unique);
                b = NurbsUtils.getUniqueKey(bb, points, unique);
                c = NurbsUtils.getUniqueKey(cc, points, unique);
                d = NurbsUtils.getUniqueKey(dd, points, unique);

                triangle = new Triangle(a, b, d);

                if (!triangle.isDegenerate(points))
                {
                    triangle.calculateNormal(points);

                    triangles[nTriangles] = triangle;
                    nTriangles++;
                }

                triangle = new Triangle(b, c, d);

                if (!triangle.isDegenerate(points))
                {
                    triangle.calculateNormal(points);

                    triangles[nTriangles] = triangle;
                    nTriangles++;
                }
            }
        }
    }

    for (j = 0; j < triangles.length; j++)
    {
        triangle = triangles[j];

        vec3.add(normals[triangle.a], normals[triangle.a], triangle.normal);
        vec3.add(normals[triangle.b], normals[triangle.b], triangle.normal);
        vec3.add(normals[triangle.c], normals[triangle.c], triangle.normal);
    }

    for (j = 0; j < normals.length; j++) {
        vec3.normalize(normals[j], normals[j]);
    }

    var result = {};

    result.points = points;
    result.lines = lines;
    result.triangles = triangles;
    result.normals = normals;
    result.uvCoords = uvCoords;

    return result;
};

/* Segment */
var SEGMENT_LENGTH = 2;

var Segment = function(a, b)
{
    this.a = a;
    this.b = b;

    this.indices = new Uint16Array([this.a, this.b]);
};

Segment.prototype.isDegenerate = function(points)
{
    var ka = NurbsUtils.computeKey(points[this.a]);
    var kb = NurbsUtils.computeKey(points[this.b]);

    return (ka === kb);
};

/* Triangle */
var TRIANGLE_LENGTH = 3;

var Triangle = function (a, b, c)
{
    this.a = a;
    this.b = b;
    this.c = c;

    this.indices = new Uint16Array([this.a, this.b, this.c]);
    this.normal = vec3.create();
};

Triangle.prototype.isDegenerate = function(points)
{
    var ka = NurbsUtils.computeKey(points[this.a]);
    var kb = NurbsUtils.computeKey(points[this.b]);
    var kc = NurbsUtils.computeKey(points[this.c]);

    return (ka === kb || kb === kc || ka === kc);
};

Triangle.prototype.calculateNormal = function(points)
{
    var a = points[this.a];
    var b = points[this.b];
    var c = points[this.c];

    var temp = vec3.create();

    vec3.subtract(this.normal, b, a);
    vec3.subtract(temp, c, a);
    vec3.cross(this.normal, this.normal, temp);
    vec3.normalize(this.normal, this.normal);
};

Triangle.prototype.update = function()
{
    this.indices.set([this.a, this.b, this.c]);
};