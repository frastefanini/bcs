var XCModel = {};

XCModel.DB_EXTENSION = "db";
XCModel.DBE_EXTENSION = "dbe";
XCModel.OBJ_EXTENSION = "obj";
XCModel.FILENAME = "FILENAME:";
XCModel.DEGREE_U_V = "DEGREE_U_V";
XCModel.NCP_U_V = "N.C.P._U_V";
XCModel.KNOTS_U_V = "KNOTS_U_V";
XCModel.COORD_CP = "COORD.C.P.(X,Y,Z,W)";
XCModel.KNOTS_U = "KNOTS_U";
XCModel.KNOTS_V = "KNOTS_V";
XCModel.FLAG_DBE = "1";
XCModel.FLAG_UNUSED = "0";
XCModel.VERSUS_COMPUTED = 0;
XCModel.VERSUS_OPPOSITE = 1;

XCModel.parseCountLine = function(line)
{
    var splittedLine = line.split('\u0020');
    var values = [];

    splittedLine.forEach(function(value)
    {
        value = value.trim();

        if (value.length > 0) {
            values.push(parseInt(value));
        }
    });

    return values;
}

XCModel.parseCPLine = function(line)
{
    var splittedLine = line.split('\u0020');
    var controlPoint = vec4.create();

    if (splittedLine.length >= 1) {
        controlPoint[0] = parseFloat(splittedLine[0]);
    }

    if (splittedLine.length >= 2) {
        controlPoint[1] = parseFloat(splittedLine[1]);
    }

    if (splittedLine.length >= 3) {
        controlPoint[2] = parseFloat(splittedLine[2]);
    }

    if (splittedLine.length >= 4) {
        controlPoint[3] = parseFloat(splittedLine[3]);
    }

    return controlPoint;
}

XCModel.parseTrimCoordsLine = function(line)
{
    var splittedLine = line.split('\u0020');
    var point = new vec2.create();

    if (splittedLine.length >= 1) {
        point[0] = parseFloat(splittedLine[0]);
    }

    if (splittedLine.length >= 2) {
        point[1] = parseFloat(splittedLine[1]);
    }

    return point;
}

XCModel.parse = function(src, name, extension)
{
    var line, i;
    var lines = src.split('\u000A');
    var index = 0;

    /* Surface file name */

    if (!lines[index].startsWith(XCModel.FILENAME)) {
        return;
    }

    index++;

    /* Introduces the surface degrees... */

    if (!lines[index].startsWith(XCModel.DEGREE_U_V)) {
        return;
    }

    /* Surface degrees */

    var values = XCModel.parseCountLine(lines[index + 1]);
    var uDegree, vDegree;

    if (values.length >= 2)
    {
        uDegree = values[0];
        vDegree = values[1];
    }

    index += 2;

    /* Introduces the control net dimensions... */

    if (!lines[index].startsWith(XCModel.NCP_U_V)) {
        return;
    }

    /* Control net dimensions */

    values = XCModel.parseCountLine(lines[index + 1]);
    var uCPCount, vCPCount;

    if (values.length >= 2)
    {
        uCPCount = values[0];
        vCPCount = values[1];
    }

    index += 2;

    /* Introduces the knot partition dimensions... */

    if (!lines[index].startsWith(XCModel.KNOTS_U_V)) {
        return;
    }

    /* Knot partition dimensions */

    values = XCModel.parseCountLine(lines[index + 1]);
    var uKnotsCount, vKnotsCount;

    if (values.length >= 2)
    {
        uKnotsCount = values[0];
        vKnotsCount = values[1];
    }

    index += 2;

    /* The 3D CPs coord. and weights by row */

    if (!lines[index].startsWith(XCModel.COORD_CP)) {
        return;
    }

    var offset = index + 1;
    var CP = [];

    for (var u = 0; u < uCPCount; u++)
    {
        CP[u] = [];

        for (var v = 0; v < vCPCount; v++)
        {
            line = lines[offset + (u * vCPCount) + v];
            line = line.trim();

            CP[u][v] = XCModel.parseCPLine(line);
        }
    }

    index += (1 + (uCPCount * vCPCount));

    /* The U knot vector in not decreasing order */

    if (!lines[index].startsWith(XCModel.KNOTS_U)) {
        return;
    }

    var uKnots = [];
    uKnots[0] = 0.0;

    for (i = 0; i < uKnotsCount; i++)
    {
        line = lines[index + 1 + i];
        line.trim();

        uKnots[i + 1] = parseFloat(line);
    }

    index += (1 + uKnotsCount);

    /* The V knot vector in not decreasing order */

    if (!lines[index].startsWith(XCModel.KNOTS_V)) {
        return;
    }

    var vKnots = [];
    vKnots[0] = 0.0;

    for (i = 0; i < vKnotsCount; i++)
    {
        line = lines[index + 1 + i];
        line.trim();

        vKnots[i + 1] = parseFloat(line);
    }

    var surface = new Surface(name, uDegree, vDegree, uKnots, vKnots, CP);
    index += (1 + vKnotsCount);

    /* This is a flag; it signals that other information follows... */

    if (extension !== XCModel.DBE_EXTENSION ||
        !lines[index].startsWith(XCModel.FLAG_DBE)) {
        return surface;
    }

    index++;

    /* Normal versus: 0 for computed versus, 1 opposite versus */

    var normalVersus = parseInt(lines[index]);

    if (normalVersus !== XCModel.VERSUS_COMPUTED &&
        normalVersus !== XCModel.VERSUS_OPPOSITE) {
        return surface;
    }

    surface.normalVersus = normalVersus;
    index++;

    /* Flag not used (see .tree format) */

    if (!lines[index].startsWith(XCModel.FLAG_UNUSED)) {
        return surface;
    }

    index++;

    /* Number of trimming curves */
    var trimCurvesCount = parseInt(lines[index]);
    index++;

    var trimCurves = [];

    for (i = 0; i < trimCurvesCount; i++)
    {
        /* Flag not used (see .tree format) */

        if (!lines[index].startsWith(XCModel.FLAG_UNUSED)) {
            continue;
        }

        index++;

        var pointsCount = parseInt(lines[index]);
        trimCurves[i] = [];

        index++;

        for (var j = 0; j < pointsCount; j++)
        {
            line = lines[index + j];
            line.trim();

            trimCurves[i][j] = XCModel.parseTrimCoordsLine(line);
        }

        index += pointsCount;
    }

    if (trimCurves.length > 0)
    {
        surface.isTrimmed = true;
        surface.trimCurves = trimCurves;
    }

    return surface;
};