var Colors = {};

Colors.rgbToHsv = function(rgb)
{
    var max = Math.max(Math.max(rgb[0], rgb[1]), rgb[2]);
    var min = Math.min(Math.min(rgb[0], rgb[1]), rgb[2]);

    var d = max - min;
    var v = max;
    var s = ((max == 0.0) ? 0.0 : (d / max));
    var h = 0.0;

    if (max != min)
    {
        switch (max)
        {
            case rgb[0]:
                h = ((rgb[1] - rgb[2]) / d) + (rgb[1] < rgb[2] ? 6.0 : 0.0);
            break;

            case rgb[1]:
                h = ((rgb[2] - rgb[0]) / d) + 2.0;
            break;

            case rgb[2]:
                h = (rgb[0] - rgb[1]) / d + 4.0;
            break;
        }

        h /= 6.0;
    }

    return vec3.fromValues(h, s, v);
};

Colors.hsvToRgb = function(hsv)
{
    var i = Math.floor(hsv[0] * 6.0);
    var f = hsv[0] * 6.0 - i;
    var p = hsv[2] * (1.0 - hsv[1]);
    var q = hsv[2] * (1.0 - f * hsv[1]);
    var t = hsv[2] * (1.0 - (1.0 - f) * hsv[1]);

    switch (i % 6)
    {
        case 0:
            return vec3.fromValues(hsv[2], t, p);
        break;

        case 1:
            return vec3.fromValues(q, hsv[2], p);
        break;

        case 2:
            return vec3.fromValues(p, hsv[2], t);
        break;

        case 3:
            return vec3.fromValues(p, q, hsv[2]);
        break;

        case 4:
            return vec3.fromValues(t, p, hsv[2]);
        break;

        case 5:
            return vec3.fromValues(hsv[2], p, q);
        break;
    }
};

Colors.randomHsv = function()
{
    var hue = Math.random();
    return vec3.fromValues(hue, 1.0, 1.0);
};